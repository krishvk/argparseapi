# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import re
import sys
import glob

for i in set(
    # Find all dirs with python file
    os.path.dirname(i) for i in glob.glob(f'../**/*.py', recursive=True)
):
    sys.path.insert(0, i)

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'ARGPARSE-ENH'
copyright = '2023, Krishna'
author = 'Krishna'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosectionlabel',
    'sphinx.ext.viewcode',
    'sphinx.ext.todo',
    'sphinx_tabs.tabs',
    'sphinx.ext.graphviz'
]

graphviz_output_format = 'svg'

autosectionlabel_prefix_document = True
autosectionlabel_maxdepth = 2

templates_path = ['_templates']
exclude_patterns = []


pygments_style = 'colorful'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

# Show TODOs in documentation
todo_include_todos = True

def setup(app):
    '''Use customizations'''
    app.add_css_file('my_theme.css')
