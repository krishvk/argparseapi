#! /usr/bin/env python3
'''
Sample argparser to demonstrate the features of argparse_enh package
'''
import argparse
import textwrap
import argparse_enh.argparse_enh as ape


def getArgParser():
    'Define and return the argparse object'

    parser = argparse.ArgumentParser(
        allow_abbrev=True,
        prog='Sample Argument Parser',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=textwrap.dedent('''\
            Sample CLI application to call an argparse script as an API

            Hello World !!! This is a multiline description of the CLI application.
            '''
        )
    )

    parser.add_argument('-i', '--integer', type=int, default=10, help='Integer')
    parser.add_argument('-s', '--string', type=str, required=True, help='String')
    parser.add_argument('-l', '--list', type=str, nargs='+', help='List of Strings')
    parser.add_argument(
        '-c', '--choices', type=str, nargs='+', choices=list('abcd'), help='List of Choices'
    )
    parser.add_argument('-r', '--rdFile', type=argparse.FileType('r'), help='Readable File')
    parser.add_argument('-w', '--wrFile', type=argparse.FileType('w'), help='Writable File')
    parser.add_argument('-q', '--quite', action='store_true', help=textwrap.dedent('''\
        A multi line Help message
        With a Second and
        A third line
        '''
        )
    )
    parser.add_argument('-v', '--version', action="version", version='1.0')

    return parser


if __name__ == '__main__':
    ape.dumpArgs(getArgParser())
    # ape.gui(getArgParser())
