#! /usr/bin/env python3
'''
Simple test to test the working of the options with count action
'''
from tests import sample_argparser
import argparse_enh.argparse_enh as ape


@ape.api(sample_argparser.getArgParser())
def coreFunction(opts=None, **kwargs): # pylint: disable=unused-argument
    '''The core function, that can also be called from other scripts as an API'''

    print(opts)


def testCountAction():
    'Calling the core function as API'

    coreFunction(string='String', integer=100)


if __name__ == '__main__':
    testCountAction()
