#! /usr/bin/env python3
'''
Simple test to demonstrate the API usage
'''

import argparse
import argparse_enh.argparse_enh as ape


def getArgParser():
    '''Returns the parser object'''

    parser = argparse.ArgumentParser(
        allow_abbrev=True,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='''Sample CLI application to call an argparse script as an API'''
    )

    parser.add_argument('-i', '--integer', type=int, default=10, help='Integer')
    parser.add_argument('-s', '--string', type=str, required=True, help='String')

    return parser


@ape.api(getArgParser())
def coreFunction(opts=None, **kwargs): # pylint: disable=unused-argument
    '''The core function, that can also be called from other scripts as an API'''

    return opts


def testApiSimple():
    'Simple test to demonstrate the usage of API'

    opts = coreFunction(string='String', integer=100)

    expected = "Namespace(integer=100, string='String')"
    actual = str(opts)
    assert expected == actual, f'Expected\n\t{expected}\nGot\n\t{actual}'



if __name__ == '__main__':
    testApiSimple()
