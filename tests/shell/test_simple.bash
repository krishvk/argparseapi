#! /usr/bin/env bash
#
# USAGE:
#       test_simple.bash -h
#
# Description:
#   Simple bash script to demonstrate how Python's argparse can be used to manage script usage
#
#
# shellcheck disable=SC2046,SC2154
eval $($(dirname $(dirname $0))/sample_argparser.py "$@")
(set ; printenv) | grep -P '^opt_'
echo Hello World >> "$opt_wrFile"
