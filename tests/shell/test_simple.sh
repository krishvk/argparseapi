#! /usr/bin/env sh
#
# USAGE:
#       test_simple.sh -h
#
# Description:
#   Simple shell(sh) script to demonstrate how Python's argparse can be used to manage script usage
#
#
# shellcheck disable=SC2046
eval $($(dirname $(dirname $0))/sample_argparser.py "$@")
(set ; printenv) | grep -P '^opt_'
