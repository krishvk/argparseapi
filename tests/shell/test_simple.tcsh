#! /usr/bin/env tcsh
#
# USAGE:
#       test_simple.tcsh -h
#
# Description:
#   Simple tcsh script to demonstrate how Python's argparse can be used to manage script usage
#
#
#
eval `$0:h:h/sample_argparser.py $argv`
(set ; printenv) | grep -P '^opt_'
