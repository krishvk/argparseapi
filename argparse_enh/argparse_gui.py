#! /usr/bin/env python3
# ------------------------------------------------
# Author:    krishna
# USAGE:
#       argparse_gui.py
# Description:
#
#
# ------------------------------------------------
import argparse

import streamlit as st
import stringcase


def __getWidget__(i: argparse.Action):
    'Return the widget for the given argparse action'

    # We access argparse actions
    # pylint: disable=protected-access

    # Convert the argument name to Title Case
    label = stringcase.titlecase(i.dest)

    # Add '*' to required arguments
    label += '*' if i.required else ''

    if i.choices is not None:
        if i.nargs in ('+', '*'):
            return st.multiselect(label, i.choices, help=i.help)
        else:
            return st.selectbox(label, i.choices, help=i.help)

    if isinstance(i, argparse._StoreAction):
        if i.type == int:
            return st.number_input(label, value=i.default, help=i.help)
        if i.type == str:
            return st.text_input(label, value=i.default, help=i.help)

    if isinstance(i, argparse._ExtendAction):
        st.write(i)
        st.write(label, i.help)

    if isinstance(i, argparse._StoreTrueAction):
        return st.checkbox(label, value=False, help=i.help)

    if isinstance(i, argparse._StoreFalseAction):
        return st.checkbox(label, value=True, help=i.help)

    # if action.nargs in ('+', '*'):
    #     args.extend(v)
    # else:
    #     args.append(str(v))

    # Do nothing for
    #   argprase._HelpAction
    #   argparse._VersionAction
    return None


def gui(parser):
    'The main function'

    st.set_page_config(page_title=parser.prog, layout='wide')

    data = dict()
    with st.form(parser.prog):
        st.title(parser.prog, help=parser.description)
        for i in parser.__dict__['_actions']:
            data[i.dest] = __getWidget__(i)

        if st.form_submit_button('Run'):
            for k, v in data.items():
                st.write(f'{k} = {v}')


if __name__ == '__main__':
    assert False, 'Meant for importing only'
