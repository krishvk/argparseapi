# ARGPARSE ENHANCEMENTS

## Description

Adds the following additional functionalities to Python's argparse.

* Argparse-API: Use the argparse script as a importable module in another Python script.
* Argparse-Shell: Use Python's argparse as the argument parser in other shell scripts.
* Argparse-GUI: Automatically create a GUI interface for the argparse script using Streamlit.

Please refer the documentation for more details.

## Installation

    pip install argparse-enh

## Documentation

For more details and usage, refer to the documentation mirrored in one of these places.

* [Github Docs](https://krishvk.github.io/argparse-enh-docs/)
* [Gitlab Docs](https://argparse-enh.gitlab.io/argparse-enh/)
* [ReadTheDocs](https://argparse-enh.readthedocs.io/en/latest/)
